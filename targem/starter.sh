#!/bin/bash -eu

declare -r LOGIN2="login"
declare -r HOST2="example.com"
declare -r PINGER="/usr/scripts/pinger.sh"
declare -r PROBECOUNT="3"
declare -r TIMESTAMP="$(date +%F\ %T)"
declare -r PAUSE="5"


do_probe() {
ssh -l ${LOGIN2} ${HOST2} "${PINGER}"
}

do_message() {
for COUNT in $(seq ${PROBECOUNT}) ; do
    echo "[${TIMESTAMP}] Выполняю Шаг${COUNT}" 
    sleep ${PAUSE}
done
}

do_probe &

do_message

wait

echo "${PINGER} on ${HOST2} завершён"

