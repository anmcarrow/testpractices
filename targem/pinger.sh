#!/bin/bash -eu

declare -r DELAY="20"
declare -r GATEWAY="$(ip r | egrep default | cut -d ' ' -f 3)"
do_ping() {
timeout ${DELAY} \
ping ${GATEWAY} > /dev/null
}

do_ping

exit 0
